package ru.edu.helpdesk.controller;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.edu.helpdesk.entity.*;
import ru.edu.helpdesk.security.HelpdeskUserPrincipal;
import ru.edu.helpdesk.service.CommentService;
import ru.edu.helpdesk.service.SupportService;
import ru.edu.helpdesk.service.TicketService;

import java.util.List;

/**
 * Контроллер для суппорта (доступен только для роли суппорта)
 */
@Slf4j
@Controller
@Secured("ROLE_SUPPORT")
@RequestMapping(value = "/support")
public class SupportController {

    private final TicketService ticketService;
    private final CommentService commentService;
    private final SupportService supportService;

    @Autowired
    public SupportController(TicketService ticketService, CommentService commentService, SupportService supportService) {
        this.ticketService = ticketService;
        this.commentService = commentService;
        this.supportService = supportService;
    }


    /**
     * Начальная страница для support
     * @param model
     * @param principal
     * @return для support только свои.
     */
    @GetMapping()
    public String index(Model model, @AuthenticationPrincipal HelpdeskUserPrincipal principal) {
        if (principal != null) {
            final User current = principal.getUser();
            if (!isRole(current)) {
                log.error("GET '/support' - ошибка доступа!");
                return "error/access_denied";
            } else {
                final List<Ticket> allTickets = supportService.allTicketsByUser(current);
                model.addAttribute("current", current);
                model.addAttribute("tickets", allTickets);
                log.info("GET '/support'");
                return "support/support";
            }

        }
        log.error("GET '/support' - ошибка доступа!");
        return "error/error";
    }

    /**
     * Отображает все ticket со статусом open
     * @param model
     * @param principal
     * @return
     */
    @GetMapping("/openTickets")
    public String working(Model model, @AuthenticationPrincipal HelpdeskUserPrincipal principal) {
        if (principal != null) {
            final User current = principal.getUser();
            if (!isRole(current)) {
                log.error("GET '/support' - ошибка доступа!");
                return "error/access_denied";
            } else {
                final List<Ticket> allTickets = supportService.allTicketByStatus(TicketStatus.OPEN);
                model.addAttribute("current", current);
                model.addAttribute("tickets", allTickets);
                log.info("GET '/support/openTickets'");
                return "support/openTickets";
            }
        }
        log.error("GET '/support/openTickets' - ошибка доступа!");
        return "error/error";
    }


    /**
     * Работа с тикетом.
     *
     * @param id
     * @param model
     * @param principal
     * @return
     */
    @GetMapping("/ticketsupport/{id}")
    public String ticketWork(@PathVariable("id") long id, Model model, @AuthenticationPrincipal HelpdeskUserPrincipal principal) {
        if (principal != null) {
            final User current = principal.getUser();
            if (!isRole(current)) {
                log.error("GET '/support' - ошибка доступа!");
                return "error/access_denied";
            } else {
                model.addAttribute("current", current);
                model.addAttribute("ticketInfo", ticketService.ticketInfo(id));
                model.addAttribute("messages", commentService.allMessageByTicketId(id));
                model.addAttribute("comment", new Comment());
                log.info("GET '/support/ticketsupport/" + id + "'");
                return "support/ticketsupport";
            }
        } else {
            log.error("GET '/support/ticketsupport/" + id + "' - ошибка доступа!");
            return "error/error";
        }
    }

    /**
     * Пост запрос для измениня стату тикета на working.
     *
     * @param id
     * @param principal
     * @return
     */
    @PostMapping("/ticketsupport/{id}")
    public String workingWithTicket(@PathVariable("id") long id, @AuthenticationPrincipal HelpdeskUserPrincipal principal) {
        if (principal != null) {
            final User current = principal.getUser();
            if (!isRole(current)) {
                log.error("GET '/support' - ошибка доступа!");
                return "error/access_denied";
            } else {
                supportService.takeTicket(ticketService.ticketInfo(id), current);
                supportService.workStatusTicket(ticketService.ticketInfo(id));
                log.info("POST '/support/ticketsupport/" + id + "'");
                String path = "redirect:/support/ticketsupport/" + id;
                return path;
            }
        } else
            log.error("POST '/support/ticketsupport/" + id + "' - ошибка доступа!");
        return "error/error";

    }

    /**
     * Пост запрос для измениня стату тикета на complete.
     *
     * @param id
     * @param principal
     * @return
     */
    @PostMapping("/ticketclose/{id}")
    public String closeTicket(@PathVariable("id") long id, @AuthenticationPrincipal HelpdeskUserPrincipal principal) {
        if (principal != null) {
            final User current = principal.getUser();
            if (!isRole(current)) {
                log.error("GET '/support' - ошибка доступа!");
                return "error/access_denied";
            } else {
                supportService.takeTicket(ticketService.ticketInfo(id), current);
                supportService.completeStatusTicket(ticketService.ticketInfo(id));
                log.info("POST '/support/ticketclose/" + id + "'");
                String path = "redirect:/support/ticketsupport/" + id;
                return path;
            }
        } else
            log.error("POST '/support/ticketclose/" + id + "' - ошибка доступа!");
        return "error/error";
    }

    /**
     * Пост запрос для измениня стату тикета на reject.
     *
     * @param id
     * @param principal
     * @return
     */
    @PostMapping("/reject/{id}")
    public String rejectTicket(@PathVariable("id") long id, @AuthenticationPrincipal HelpdeskUserPrincipal principal) {
        if (principal != null) {
            final User current = principal.getUser();
            if (!isRole(current)) {
                log.error("GET '/support' - ошибка доступа!");
                return "error/access_denied";
            } else {
                supportService.takeTicket(ticketService.ticketInfo(id), current);
                supportService.rejectedStatusTicket(ticketService.ticketInfo(id));
                log.info("POST '/support/reject/" + id + "'");
                String path = "redirect:/support/ticketsupport/" + id;
                return path;
            }
        } else
            log.error("POST '/support/reject/" + id + "' - ошибка доступа!");
        return "error/error";

    }

    /**
     * Пост запролс для создания коментария к ticket.
     *
     * @param id
     * @param comment
     * @param principal
     * @return
     */
    @PostMapping("/ticket/{id}")
    public String createComment(@PathVariable("id") long id, @ModelAttribute Comment comment, @AuthenticationPrincipal HelpdeskUserPrincipal principal) {
        if (principal != null) {
            final User current = principal.getUser();
            if (!isRole(current)) {
                log.error("GET '/support' - ошибка доступа!");
                return "error/access_denied";
            } else {
                comment.setTicket(ticketService.ticketInfo(id));
                comment.setUser(current);
                comment.setId(null);
                commentService.createComment(comment);
                log.info("POST '/support/ticket/" + id + "'");
                String path = "redirect:/support/ticketsupport/" + id;
                return path;
            }
        } else
            log.error("POST '/support/ticket/" + id + "' - ошибка доступа!");
        return "error/error";
    }

    /**
     * Метод запроса соответствия роли, из БД.
     */

    private boolean isRole(User user) {
        return supportService.getRole(user).equals(UserRole.SUPPORT);
    }
}
