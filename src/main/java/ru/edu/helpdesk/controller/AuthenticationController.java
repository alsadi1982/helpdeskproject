package ru.edu.helpdesk.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.edu.helpdesk.entity.User;
import ru.edu.helpdesk.service.AuthenticationService;


/**
 * Контроллер для регистрации в системе поддержки и начала работы с ней
 * Приветственная страничка, приглашение залогиниться, приглашение зарегистрироваться
 */
@Slf4j
@Controller
public class AuthenticationController {

    private boolean passMatch = true;
    private boolean userExist = false;

    private final AuthenticationService authenticationService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public AuthenticationController(AuthenticationService authenticationService, PasswordEncoder passwordEncoder) {
        this.authenticationService = authenticationService;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * Get запрос рендерит приведсвенную страницу
     *
     * @return
     */
    @GetMapping("/")
    public String welcome() {
        passMatch = true;
        userExist = false;
        return "welcome";
    }


    /**
     * Get запрос рендерит страницу с формой регистрации пользователя
     *
     * @param model
     * @return
     */
    @GetMapping("/registration")
    public String registrationForm(Model model) {
        model.addAttribute("passMatch", passMatch);
        model.addAttribute("userExist", userExist);
        log.info("GET '/registration'");
        return "registration";
    }

    /**
     * Post запрос регистрирует новго пользователя,проверяет правильность ввода пороля, проверяет уникальность логина
     * в случае ошибки при вводе пароля или если логин совпадает с логином уже зарегистрированного пользователя выводит соответствующее предупреждающее сообщение и сбрасывает форму регистрации
     *
     * @param firstname - имя пользователя
     * @param lastname  - фамилия пользователя
     * @param userName  - уникальный логин
     * @param password1 - придуманный пароль пользователя
     * @param password2 - проверка ввода пароля
     */
    @PostMapping("/registration")
    public String registration(@RequestParam("firstname") String firstname,
                               @RequestParam("lastname") String lastname,
                               @RequestParam("userName") String userName,
                               @RequestParam("password1") String password1,
                               @RequestParam("password2") String password2) {


        if (authenticationService.getUserByUsername(userName) != null) {
            userExist = true;
            passMatch = true;
            log.error("POST '/registration' - login already exists");
            return "redirect:/registration";
        } else if (!password1.equals(password2)) {
            passMatch = false;
            userExist = false;
            log.error("POST '/registration' - password1 != password2");
            return "redirect:/registration";
        } else {
            String hashedPassword = passwordEncoder.encode(password1);
            User user = new User();
            user.setFirstName(firstname);
            user.setLastName(lastname);
            user.setLogin(userName);
            user.setPassword(hashedPassword);

            authenticationService.saveUser(user);
            log.info("POST '/registration' - successful registration");
            return "redirect:/login";
        }
    }

        /**
         * Страница для забаненного пользователя
         *
         * @return
         */
        @GetMapping("/banned")
        public String fromBanned() {
            return "banned";
        }


}
