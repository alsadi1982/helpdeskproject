package ru.edu.helpdesk.entity;

/**
 * Статусы, в которых может находиться обращение
 */
public enum TicketStatus {
    OPEN,
    WORKING,
    COMPLETED,
    REJECTED

}
