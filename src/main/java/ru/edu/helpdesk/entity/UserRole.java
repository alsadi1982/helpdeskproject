package ru.edu.helpdesk.entity;

/**
 * Возможные роли клиентов
 */
public enum UserRole {
    ADMIN,
    SUPPORT,
    USER,
    BANNED
}