package ru.edu.helpdesk.service;

import ru.edu.helpdesk.entity.Title;
import ru.edu.helpdesk.entity.User;
import ru.edu.helpdesk.entity.UserRole;
import ru.edu.helpdesk.form.UserEditForm;

import java.util.List;

public interface AdminService {

    List<User> allUsers();

    void saveUser(User user);

    User getUserByUsername(String username);

    User getUserById(Long id);

    void updateUser(UserEditForm userEditForm);

    List<Title> allTitles();

    Title getTitleById(Long id);

    void saveTitle(Title title);

    void deleteTitleById(Long id);

    /**
     * Поиск юзера по id
     */
    UserRole getRole(User user);
}
