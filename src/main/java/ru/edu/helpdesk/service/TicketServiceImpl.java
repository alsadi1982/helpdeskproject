package ru.edu.helpdesk.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.edu.helpdesk.entity.Ticket;
import ru.edu.helpdesk.entity.Title;
import ru.edu.helpdesk.entity.User;
import ru.edu.helpdesk.entity.UserRole;
import ru.edu.helpdesk.repository.TicketRepository;
import ru.edu.helpdesk.repository.TitleRepository;
import ru.edu.helpdesk.repository.UserRepository;

import java.util.List;

/**
 * Сервис, реализующий связь между таблицей ticket базы данных и интерфейса
 */
@Service
public class TicketServiceImpl implements TicketService {

    private final TicketRepository ticketRepository;
    private final TitleRepository titleRepository;
    private final UserRepository userRepository;

    @Autowired
    public TicketServiceImpl(TicketRepository ticketRepository, TitleRepository titleRepository, UserRepository userRepository) {
        this.ticketRepository = ticketRepository;
        this.titleRepository = titleRepository;
        this.userRepository = userRepository;
    }

    /**
     * Метод TicketServiceImpl#createTicket(Ticket ticket) создает ticket и добавляет его в БД
     *
     * @param ticket новый ticket
     */
    @Override
    public void createTicket(Ticket ticket) {

        ticketRepository.save(ticket);
    }

    /**
     * Метод TicketServiceImpl#ticketInfo(long id) просматривает информацию по ticket
     *
     * @param id нужного нам ticket
     * @return Ticket искомый нами ticket
     */
    @Override
    public Ticket ticketInfo(long id) {
        return ticketRepository.getById(id);
    }

    /**
     * найти все ticket по id клиента
     */
    @Override
    public List<Ticket> allTicketsByClientId(long clientId) {
        return ticketRepository.getAllByClient_Id(clientId);
    }

    /**
     * найти все title
     */
    @Override
    public List<Title> allTitles() {
        return titleRepository.findAllByMarkFalse();
    }

    /**
     * Запрос на проверку роли.
     * @param user
     * @return
     */
    @Override
    public UserRole getRole(User user) {
        return userRepository.findByLogin(user.getLogin()).getRole();
    }
}
