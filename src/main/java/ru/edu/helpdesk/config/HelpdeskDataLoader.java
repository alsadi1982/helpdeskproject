package ru.edu.helpdesk.config;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.edu.helpdesk.entity.*;
import ru.edu.helpdesk.repository.CommentRepository;
import ru.edu.helpdesk.repository.TicketRepository;
import ru.edu.helpdesk.repository.TitleRepository;
import ru.edu.helpdesk.repository.UserRepository;

/**
 * Имплементируем интерфейс ApplicationRunner для наполнения таблиц в базе данных при запуске приложения
 */
@Component
public class HelpdeskDataLoader implements ApplicationRunner {
    private final UserRepository userRepository;
    private final TicketRepository ticketRepository;
    private final CommentRepository commentRepository;
    private final PasswordEncoder passwordEncoder;
    private final TitleRepository titleRepository;

    public HelpdeskDataLoader(UserRepository userRepository,
                              TicketRepository ticketRepository,
                              CommentRepository commentRepository,
                              PasswordEncoder passwordEncoder,
                              TitleRepository titleRepository) {
        this.userRepository = userRepository;
        this.ticketRepository = ticketRepository;
        this.commentRepository = commentRepository;
        this.passwordEncoder = passwordEncoder;
        this.titleRepository = titleRepository;
    }

    /**
     * Наполнение таблиц в базе данных при запуске приложения
     */
    public void run(ApplicationArguments args) {

        if (userRepository.count() > 0) {
            return;
        }

        createUser("admin", "admin", UserRole.ADMIN, "Alex", "Sadikov");
        User support = createUser("support", "support", UserRole.SUPPORT, "Artem", "Mikishev");
        User user1 = createUser("user1", "user1", UserRole.USER, "Olga", "Lapenok");
        User user2 = createUser("user2", "user2", UserRole.USER, "Sergey", "Malyshev");

        Title pc = createTitle("Проблема с PC", false);
        Title network = createTitle("Проблема с NETWORK", false);
        Title printer = createTitle("Проблема с PRINTER", false);
        createTitle("УДАЛЕНО! не использовать!", true);

        Ticket ticket1 = createTicket(pc, "Не загружается компьютер", TicketStatus.WORKING, user1, support);
        Ticket ticket2 = createTicket(network, "Медленно работает сеть", TicketStatus.WORKING, user1, support);
        Ticket ticket3 = createTicket(network, "Не работает wifi", TicketStatus.OPEN, user2, null);
        createTicket(printer, "Сломался принтер", TicketStatus.OPEN, user1, null);
        Ticket ticket5 = createTicket(pc, "Необходимо установить ПО", TicketStatus.WORKING, user2, support);
        createTicket(printer, "Закончилась краска", TicketStatus.OPEN, user2, null);

        createComment(ticket1, support, "Запрос доп информации по заявке ticket1");
        createComment(ticket2, support, "Запрос доп информации по заявке ticket2");
        createComment(ticket1, user1, "Подробное описание проблемы по заявке ticket1");
        createComment(ticket2, user1, "Подробное описание проблемы по заявке ticket2");
        createComment(ticket3, support, "Запрос доп информации по заявке ticket3");
        createComment(ticket5, support, "Запрос доп информации по заявке ticket5");
    }

    /**
     * Метод создает категории и сохраняет в базе данных.
     */
    private Title createTitle(String name, Boolean mark) {
        Title title = new Title();
        title.setName(name);
        title.setMark(mark);
        return titleRepository.save(title);
    }

    /**
     * Метод создает пользователя и сохраняет в базе данных.
     */
    private User createUser(String login, String password, UserRole role, String firstName, String lastName) {
        String hashedPassword = passwordEncoder.encode(password);
        User user = new User();
        user.setLogin(login);
        user.setPassword(hashedPassword);
        user.setRole(role);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        return userRepository.save(user);
    }

    /**
     * Метод создает заявку и сохраняет в базе данных.
     */
    private Ticket createTicket(Title title, String description, TicketStatus status, User client, User support) {
        Ticket ticket = new Ticket();
        ticket.setTitle(title);
        ticket.setDescription(description);
        ticket.setStatus(status);
        ticket.setClient(client);
        ticket.setSupport(support);
        return ticketRepository.save(ticket);
    }

    /**
     * Метод создает комментарии и сохраняет в базе данных.
     */
    private Comment createComment(Ticket ticket, User user, String message) {
        Comment comment = new Comment();
        comment.setTicket(ticket);
        comment.setUser(user);
        comment.setMessage(message);
        return commentRepository.save(comment);
    }
}
