package ru.edu.helpdesk.controller;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.Model;
import ru.edu.helpdesk.entity.*;
import ru.edu.helpdesk.security.HelpdeskUserPrincipal;
import ru.edu.helpdesk.service.CommentService;
import ru.edu.helpdesk.service.SupportService;
import ru.edu.helpdesk.service.TicketService;

import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class SupportControllerTest {

    @Autowired
    private SupportController supportController;

    @MockBean
    private TicketService ticketService;

    @MockBean
    private CommentService commentService;

    @MockBean
    private SupportService supportService;


    @Mock
    private Model model;

    private User support = new User();
    private Ticket ticket = new Ticket();
    private final List<Ticket> allTickets = Collections.singletonList(ticket);

    @TestConfiguration
    static class Config {
        @Bean
        public SupportController supportController(TicketService ticketService, CommentService commentService, SupportService supportService) {
            return new SupportController(ticketService, commentService, supportService);
        }
    }

    @Before
    public void before() {

        Mockito.when(supportService.allTicketByStatus(TicketStatus.OPEN)).thenReturn(allTickets);
        Mockito.when(supportService.allTicketsByUser(support)).thenReturn(allTickets);
        Mockito.when(supportService.takeTicket(ticketService.ticketInfo(1L), support)).thenReturn(ticket);
        Mockito.when(supportService.workStatusTicket(ticketService.ticketInfo(1L))).thenReturn(ticket);
        Mockito.when(supportService.completeStatusTicket(ticketService.ticketInfo(1L))).thenReturn(ticket);
        Mockito.when(supportService.rejectedStatusTicket(ticketService.ticketInfo(1L))).thenReturn(ticket);
        Mockito.when(supportService.getRole(support)).thenReturn(UserRole.SUPPORT);

    }


    /**
     * Позитивный сценарий выполнения метода index() должен вернуть путь к html файлу support
     */
    @Test
    public void testIndexPositive() {
        HelpdeskUserPrincipal principal = new HelpdeskUserPrincipal(support);
        String returnValue = supportController.index(model, principal);
        Assert.assertEquals("support/support", returnValue);
    }

    /**
     * Негативный сценарий выполнения метода index() должен вернуть путь к html файлу error
     */
    @Test
    public void testIndexNegative() {
        HelpdeskUserPrincipal principal = null;
        verify(model, times(0)).addAttribute("current", support);
        verify(model, times(0)).addAttribute("tickets", allTickets);
        String returnValue = supportController.index(model, principal);
        Assert.assertEquals("error/error", returnValue);
    }


    /**
     * Позитивный сценарий выполнения метода working() должен вернуть путь к html файлу openTickets
     */
    @Test
    public void testWorkingPositive() {

        HelpdeskUserPrincipal principal = new HelpdeskUserPrincipal(support);
        String returnValue = supportController.working(model, principal);
        verify(model, times(1)).addAttribute("current", support);
        verify(model, times(1)).addAttribute("tickets", allTickets);
        Assert.assertEquals("support/openTickets", returnValue);

    }

    /**
     * Негативный сценарий выполнения метода working() должен вернуть путь к html файлу error
     */
    @Test
    public void workingTestNegative() {

        String returnValue = supportController.working(model, null);
        verify(model, times(0)).addAttribute("current", support);
        verify(model, times(0)).addAttribute("tickets", allTickets);
        Assert.assertEquals("error/error", returnValue);
    }

    /**
     * Позитивный сценарий выполнения метода ticketWork() должен вернуть путь к html файлу ticketsupport
     */
    @Test
    public void testTicketWorkPositive() {
        HelpdeskUserPrincipal principal = new HelpdeskUserPrincipal(support);
        ticket.setId(1L);
        String returnValue = supportController.ticketWork(1L, model, principal);
        verify(model, times(1)).addAttribute("current", support);
        verify(model, times(1)).addAttribute("ticketInfo", ticketService.ticketInfo(1L));
        verify(model, times(1)).addAttribute("messages", commentService.allMessageByTicketId(1L));
        Assert.assertEquals("support/ticketsupport", returnValue);
    }

    /**
     * Негативный сценарий выполнения метода ticketWork() должен вернуть путь к html файлу error
     */
    @Test
    public void testTicketWorkNegative() {
        ticket.setId(1L);
        String returnValue = supportController.ticketWork(1L, model, null);
        verify(model, times(0)).addAttribute("current", support);
        verify(model, times(0)).addAttribute("ticketInfo", ticketService.ticketInfo(1L));
        verify(model, times(0)).addAttribute("messages", commentService.allMessageByTicketId(1L));
        Assert.assertEquals("error/error", returnValue);
    }

    /**
     * Позитивный сценарий выполнения метода workWithTicket() должен вернуть GET запрос  /ticketsupport/{id}
     */
    @Test
    public void workingWithTicketPositive() {
        HelpdeskUserPrincipal principal = new HelpdeskUserPrincipal(support);
        ticket.setId(1L);
        String returnValue = supportController.workingWithTicket(1L, principal);
        Assert.assertEquals("redirect:/support/ticketsupport/1", returnValue);
    }

    /**
     * Негативный сценарий выполнения метода workWithTicket() должен вернуть путь к html файлу error
     */
    @Test
    public void workingWithTicketNegative() {
        ticket.setId(1L);
        String returnValue = supportController.workingWithTicket(1L, null);
        Assert.assertEquals("error/error", returnValue);
    }

    /**
     * Позитивный сценарий выполнения метода closeTicket() должен вернуть GET запрос  /ticketsupport/{id}
     */
    @Test
    public void closeTicketPositive() {
        HelpdeskUserPrincipal principal = new HelpdeskUserPrincipal(support);
        ticket.setId(1L);
        String returnValue = supportController.closeTicket(1L, principal);
        Assert.assertEquals("redirect:/support/ticketsupport/1", returnValue);
    }

    /**
     * Негативный сценарий выполнения метода closeTicket() должен вернуть путь к html файлу error
     */
    @Test
    public void closeTicketNegative() {
        ticket.setId(1L);
        String returnValue = supportController.closeTicket(1L, null);
        Assert.assertEquals("error/error", returnValue);
    }

    /**
     * Позитивный сценарий выполнения метода rejectTicket() должен вернуть GET запрос  /ticketsupport/{id}
     */
    @Test
    public void rejectTicketPositive() {
        HelpdeskUserPrincipal principal = new HelpdeskUserPrincipal(support);
        ticket.setId(1L);
        String returnValue = supportController.rejectTicket(1L, principal);
        Assert.assertEquals("redirect:/support/ticketsupport/1", returnValue);
    }

    /**
     * Негативный сценарий выполнения метода rejectTicket() должен вернуть путь к html файлу error
     */
    @Test
    public void rejectTicketNegative() {
        ticket.setId(1L);
        String returnValue = supportController.rejectTicket(1L, null);
        Assert.assertEquals("error/error", returnValue);
    }

    /**
     * Позитивный сценарий выполнения метода createComment() должен вернуть GET запрос  /ticketsupport/{id}
     */
    @Test
    public void createCommentPositive() {
        HelpdeskUserPrincipal principal = new HelpdeskUserPrincipal(support);
        ticket.setId(1L);
        String returnValue = supportController.createComment(1L, new Comment(), principal);
        Assert.assertEquals("redirect:/support/ticketsupport/1", returnValue);
    }

    /**
     * Негативный сценарий выполнения метода createComment() должен вернуть путь к html файлу error
     */
    @Test
    public void createCommentNegative() {
        ticket.setId(1L);
        String returnValue = supportController.createComment(1L, new Comment(), null);
        Assert.assertEquals("error/error", returnValue);
    }

}
