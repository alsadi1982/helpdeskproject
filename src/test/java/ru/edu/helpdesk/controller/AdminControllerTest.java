package ru.edu.helpdesk.controller;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.ui.Model;
import ru.edu.helpdesk.entity.Title;
import ru.edu.helpdesk.entity.User;
import ru.edu.helpdesk.entity.UserRole;
import ru.edu.helpdesk.form.UserEditForm;
import ru.edu.helpdesk.security.HelpdeskUserPrincipal;
import ru.edu.helpdesk.service.AdminService;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

/**
 * AdminController Test
 */
@ExtendWith(MockitoExtension.class)
public class AdminControllerTest {

    @InjectMocks
    private AdminController adminController;

    @Mock
    private AdminService adminService;

    @Mock
    private Model model;

    private final User ADMIN = new User(1L, "admin", "admin", "admin", "admin", UserRole.ADMIN);
    private final User SUPPORT = new User(2L, "support", "support", "support", "support", UserRole.SUPPORT);
    private final User USER1 = new User(3L, "user1", "user1", "user1", "user1", UserRole.USER);
    private final User USER2 = new User(4L, "user2", "user2", "user2", "user2", UserRole.USER);
    private final List<User> USERS = Arrays.asList(ADMIN, SUPPORT, USER1, USER2);
    private final HelpdeskUserPrincipal PRINCIPAL = new HelpdeskUserPrincipal(ADMIN);
    private final Title PC = new Title(1L, "Проблема с PC", false);
    private final Title NETWORK = new Title(1L, "Проблема с NETWORK", false);
    private final Title PRINTER = new Title(1L, "Проблема с PRINTER", false);
    private final List<Title> TITLES = Arrays.asList(PC, NETWORK, PRINTER);


    /**
     * Сценарий выполнения метода index() должен вернуть путь к html файлу admin/index
     * а также заполнить атрибут модели current
     */
    @Test
    public void testIndex() {
        when(adminService.getRole(PRINCIPAL.getUser())).thenReturn(UserRole.ADMIN);
        String returnValue = adminController.index(model, PRINCIPAL);
        assertEquals("admin/index", returnValue);
        verify(model, times(1)).addAttribute("current", ADMIN);
    }

    /**
     * Сценарий выполнения метода index() должен вернуть путь к html файлу admin/index
     * а также заполнить атрибут модели users, а current не должен быть заполнен
     */
    @Test
    public void testIndexWithoutPrincipal() {
        String returnValue = adminController.index(model, null);
        verify(model, never()).addAttribute(eq("current"), any());
        assertEquals("admin/index", returnValue);
    }

    /**
     * Сценарий выполнения метода userList() должен вернуть путь к html файлу admin/user_list
     * а также заполнить атрибуты модели current и users
     */
    @Test
    public void testUserList() {
        when(adminService.allUsers()).thenReturn(USERS);
        when(adminService.getRole(PRINCIPAL.getUser())).thenReturn(UserRole.ADMIN);
        String returnValue = adminController.userList(model, PRINCIPAL);
        verify(model, times(1)).addAttribute("current", ADMIN);
        verify(model, times(1)).addAttribute("users", USERS);
        assertEquals("admin/user_list", returnValue);
    }

    /**
     * Сценарий выполнения метода userList() должен вернуть путь к html файлу admin/user_list
     * а также заполнить атрибут модели users, а current не должен быть заполнен
     */
    @Test
    public void testUserListWithoutPrincipal() {
        when(adminService.allUsers()).thenReturn(USERS);

        String returnValue = adminController.userList(model, null);
        verify(model, never()).addAttribute(eq("current"), any());
        verify(model, times(1)).addAttribute("users", USERS);
        assertEquals("admin/user_list", returnValue);
    }

    /**
     * Сценарий выполнения метода userEdit() должен вернуть путь к html файлу admin/user_edit
     * а также заполнить атрибуты модели current и user
     */
    @Test
    public void testUserEdit() {
        when(adminService.getUserById(USER1.getId())).thenReturn(USER1);
        when(adminService.getRole(PRINCIPAL.getUser())).thenReturn(UserRole.ADMIN);
        String returnValue = adminController.userEdit(model, PRINCIPAL, USER1.getId());
        verify(model, times(1)).addAttribute("current", ADMIN);
        verify(model, times(1)).addAttribute("user", USER1);
        assertEquals("admin/user_edit", returnValue);
    }

    /**
     * Сценарий выполнения метода userEdit() должен вернуть путь к html файлу admin/user_edit
     * а также заполнить атрибут модели user, а current не должен быть заполнен
     */
    @Test
    public void testUserEditWithoutPrincipal() {
        when(adminService.getUserById(USER1.getId())).thenReturn(USER1);

        String returnValue = adminController.userEdit(model, null, USER1.getId());
        verify(model, never()).addAttribute(eq("current"), any());
        verify(model, times(1)).addAttribute("user", USER1);
        assertEquals("admin/user_edit", returnValue);
    }

    /**
     * Сценарий выполнения метода userEditPost() должен вернуть redirect:/admin/user
     */
    @Test
    public void testUserEditPost() {
        UserEditForm userEditForm = new UserEditForm();
        String returnValue = adminController.userEditPost(userEditForm);
        verify(adminService, times(1)).updateUser(userEditForm);
        assertEquals("redirect:/admin/user", returnValue);
    }

    /**
     * Сценарий выполнения метода titleList() должен вернуть путь к html файлу admin/title_list
     * а также заполнить атрибуты модели current и titles
     */
    @Test
    public void testTitleList() {
        when(adminService.allTitles()).thenReturn(TITLES);
        when(adminService.getRole(PRINCIPAL.getUser())).thenReturn(UserRole.ADMIN);
        String returnValue = adminController.titleList(model, PRINCIPAL);
        verify(model, times(1)).addAttribute("current", ADMIN);
        verify(model, times(1)).addAttribute("titles", TITLES);
        assertEquals("admin/title_list", returnValue);
    }

    /**
     * Сценарий выполнения метода titleEdit() должен вернуть путь к html файлу admin/title_edit
     * а также заполнить атрибуты модели current и title
     */
    @Test
    public void testTitleEdit() {
        when(adminService.getTitleById(NETWORK.getId())).thenReturn(NETWORK);
        when(adminService.getRole(PRINCIPAL.getUser())).thenReturn(UserRole.ADMIN);
        String returnValue = adminController.titleEdit(model, PRINCIPAL, NETWORK.getId());
        verify(model, times(1)).addAttribute("current", ADMIN);
        verify(model, times(1)).addAttribute("title", NETWORK);
        assertEquals("admin/title_edit", returnValue);
    }

    /**
     * Сценарий выполнения метода titleEditPost() должен вернуть redirect:/admin/title
     */
    @Test
    public void testTitleEditPost() {
        Title title = new Title();
        String returnValue = adminController.titleEditPost(title);
        verify(adminService, times(1)).saveTitle(title);
        assertEquals("redirect:/admin/title", returnValue);
    }

    /**
     * Сценарий выполнения метода titleDelete() должен вернуть путь к html файлу admin/title_list
     * а также заполнить атрибуты модели current и titles
     */
    @Test
    public void testTitleDelete() {
        when(adminService.allTitles()).thenReturn(TITLES);
        when(adminService.getRole(PRINCIPAL.getUser())).thenReturn(UserRole.ADMIN);
        String returnValue = adminController.titleDelete(model, PRINCIPAL, NETWORK.getId());
        verify(model, times(1)).addAttribute("current", ADMIN);
        verify(model, times(1)).addAttribute("titles", TITLES);
        verify(model, never()).addAttribute(eq("message"), any());
        assertEquals("admin/title_list", returnValue);
    }

    /**
     * Негативный сценарий выполнения метода titleDelete() должен вернуть путь к html файлу admin/title_list
     * а также заполнить атрибуты модели current, titles и message
     */
    @Test
    public void testTitleDeleteNegative() {
        when(adminService.allTitles()).thenReturn(TITLES);
        doThrow(new RuntimeException()).when(adminService).deleteTitleById(NETWORK.getId());
        when(adminService.getRole(PRINCIPAL.getUser())).thenReturn(UserRole.ADMIN);
        String returnValue = adminController.titleDelete(model, PRINCIPAL, NETWORK.getId());
        verify(model, times(1)).addAttribute("current", ADMIN);
        verify(model, times(1)).addAttribute("titles", TITLES);
        verify(model, times(1)).addAttribute(eq("message"), any());
        assertEquals("admin/title_list", returnValue);
    }

    /**
     * Сценарий выполнения метода titleNew() должен вернуть путь к html файлу admin/title_new
     * а также заполнить атрибуты модели current и title
     */
    @Test
    public void testTitleNew() {
        when(adminService.getRole(PRINCIPAL.getUser())).thenReturn(UserRole.ADMIN);
        String returnValue = adminController.titleNew(model, PRINCIPAL);
        verify(model, times(1)).addAttribute("current", ADMIN);
        verify(model, times(1)).addAttribute(eq("title"), any());
        assertEquals("admin/title_new", returnValue);
    }

    /**
     * Сценарий выполнения метода titleNewPost() должен вернуть redirect:/admin/title
     */
    @Test
    public void testTitleNewPost() {
        Title title = new Title();
        String returnValue = adminController.titleNewPost(title);
        verify(adminService, times(1)).saveTitle(title);
        assertEquals("redirect:/admin/title", returnValue);
    }
}
