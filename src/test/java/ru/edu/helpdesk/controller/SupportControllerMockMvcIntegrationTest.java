package ru.edu.helpdesk.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Интеграционный тест для SupportController
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SupportControllerMockMvcIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    /**
     * GET запрос на /support
     * успешный тест с ролью SUPPORT
     * так как доступ к /support есть только у роли SUPPORT
     */
    @Test
    @WithUserDetails("support")
    public void supportViewSuccessTest() throws Exception {
        this.mockMvc.perform(get("/support"))
                .andExpect(status().isOk());

    }

    /**
     * GET запрос на /support
     * тест c анонимным пользователем, происходит перенаправление на страницу /login
     */
    @Test
    public void supportViewNegativeTest() throws Exception {
        this.mockMvc.perform(get("/support"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("http://localhost/login"));

    }

    /**
     * Проверка формы авторизации - успешный сценарий
     * В базе данных есть пользователь с ролью SUPPORT (логин support, пароль support)
     * после успешной авторизации происходит перенаправление на главную страницу /support
     */
    @Test
    public void loginFormTest() throws Exception {
        this.mockMvc.perform(formLogin().user("support").password("support"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/support"));

    }

    /**
     * Проверка формы авторизации - негативный сценарий
     * В базе данных нет пользователя с такими параметрами
     */
    @Test
    public void loginNegativeTest() throws Exception {
        this.mockMvc.perform(post("/login").param("test", "test"))
                .andExpect(status().isForbidden());


    }

    /**
     * GET запрос на /support/openTickets
     * успешный тест с ролью SUPPORT
     */
    @Test
    @WithUserDetails("support")
    public void supportInfoSuccessTest() throws Exception {
        this.mockMvc.perform(get("/support/openTickets"))
                .andExpect(status().isOk());

    }

    /**
     * GET запрос на /support/ticketsupport/{id}
     * успешный тест с ролью SUPPORT
     */
    @Test
    @WithUserDetails("support")
    public void supportNewSuccessTest() throws Exception {
        this.mockMvc.perform(get("/support/ticketsupport/{id}", 9L))
                .andExpect(status().isOk());

    }


}
