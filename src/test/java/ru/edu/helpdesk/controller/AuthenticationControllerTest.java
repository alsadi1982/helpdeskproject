package ru.edu.helpdesk.controller;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.Model;
import ru.edu.helpdesk.entity.User;
import ru.edu.helpdesk.entity.UserRole;
import ru.edu.helpdesk.service.AuthenticationService;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

/**
 * Тестируем контроллер AuthenticationController (модульное тестирование)
 */
@RunWith(SpringRunner.class)
public class AuthenticationControllerTest {

    @Autowired
    private AuthenticationController authenticationController;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @MockBean
    private AuthenticationService authenticationService;

    @Mock
    private Model model;

    private final User USER1 = new User(3L, "user1", "user1", "user1", "user1", UserRole.USER);

    @TestConfiguration
    static class Config {

        @Bean
        public AuthenticationController authenticationController(AuthenticationService authenticationService, PasswordEncoder passwordEncoder) {
            return new AuthenticationController(authenticationService, passwordEncoder);
        }
    }

    /**
     * Успешный сценарий выполнения метода AuthenticationController#welcome() возвращает путь к странице welcome.html
     */
    @Test
    public void welcome_Test() {

        String actual = authenticationController.welcome();
        Assertions.assertEquals("welcome", actual);
    }

    /**
     * Успешный сценарий выполнения метода AuthenticationController#welcome() возвращает путь к странице welcome.html
     */
    @Test
    public void fromBanned_Test() {

        String actual = authenticationController.fromBanned();
        Assertions.assertEquals("banned", actual);
    }

    /**
     * Успешный сценарий выполнения метода AuthenticationController#registrationForm(Model model) возвращает путь к странице registration.html
     */
    @Test
    public void registrationForm_Test() {

        String actual = authenticationController.registrationForm(model);

        verify(model, times(1)).addAttribute(eq("passMatch"), any());
        verify(model, times(1)).addAttribute(eq("userExist"), any());

        Assertions.assertEquals("registration", actual);
    }

    /**
     * Успешный сценарий выполнения метода AuthenticationController#registration(String firstname, String lastname, String userName, String password1, String password2)
     * перенаправляет GET запрос на /login
     */
    @Test
    public void registration_Success_Test() {

        String firstName = "user1";
        String lastName = "user1";
        String userName = "user1";
        String password1 = "user1";
        String password2 = "user1";
        String actual = authenticationController.registration(firstName, lastName, userName, password1, password2);

        verify(passwordEncoder, times(1)).encode(password1);
        verify(authenticationService, times(1)).saveUser(any());

        Assertions.assertEquals("redirect:/login", actual);

    }

    /**
     * Негативный сценарий выполнения метода AuthenticationController#registration(String firstname, String lastname, String userName, String password1, String password2)
     * если пользователь с таким login же существует
     * перенаправляет GET запрос на /registration
     */
    @Test
    public void registration_Fail_LoginAlreadyExist_Test() {

        String firstName = "user1";
        String lastName = "user1";
        String userName = "user1";
        String password1 = "user1";
        String password2 = "user1";
        when(authenticationService.getUserByUsername(userName)).thenReturn(USER1);
        String actual = authenticationController.registration(firstName, lastName, userName, password1, password2);

        verify(authenticationService, times(1)).getUserByUsername(userName);

        Assertions.assertEquals("redirect:/registration", actual);

    }

    /**
     * Негативный сценарий выполнения метода AuthenticationController#registration(String firstname, String lastname, String userName, String password1, String password2)
     * если пользователь допустил ошибку при повторном вводе пароля
     * перенаправляет GET запрос на /registration
     */
    @Test
    public void registration_Fail_PasswordDontMatch_Test() {

        String firstName = "user1";
        String lastName = "user1";
        String userName = "user1";
        String password1 = "user1";
        String password2 = "user2";
        String actual = authenticationController.registration(firstName, lastName, userName, password1, password2);

        Assertions.assertEquals("redirect:/registration", actual);

    }

}
