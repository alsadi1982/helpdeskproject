package ru.edu.helpdesk.controller;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.Model;
import ru.edu.helpdesk.entity.Comment;
import ru.edu.helpdesk.entity.Ticket;
import ru.edu.helpdesk.entity.User;
import ru.edu.helpdesk.entity.UserRole;
import ru.edu.helpdesk.security.HelpdeskUserPrincipal;
import ru.edu.helpdesk.service.CommentServiceImpl;
import ru.edu.helpdesk.service.TicketServiceImpl;

import java.util.Collections;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;


/**
 * Тестируем контроллер TicketController (модульное тестирование)
 */

@RunWith(SpringRunner.class)
public class TicketControllerTest {


    @MockBean
    private TicketServiceImpl ticketService;

    @MockBean
    private CommentServiceImpl commentService;

    @Autowired
    private TicketController ticketController;

    @TestConfiguration
    static class Config {

        @Bean
        public TicketController ticketController(TicketServiceImpl ticketService, CommentServiceImpl commentService) {
            return new TicketController(ticketService, commentService);
        }
    }


    @Mock
    private Model model;

    private final User user = new User();
    private final Ticket ticket = new Ticket();
    private final Comment comment = new Comment();
    private final List<Ticket> allTickets = Collections.singletonList(ticket);


    @Before
    public void before() {
        when(ticketService.getRole(user)).thenReturn(UserRole.USER);
        Mockito.when(ticketService.allTicketsByClientId(anyLong())).thenReturn(allTickets);
        Mockito.when(ticketService.ticketInfo(1L)).thenReturn(ticket);

    }

    /**
     * Позитивный сценарий выполнения метода ticketView() должен вернуть путь к html файлу ticket
     */
    @Test
    public void ticketViewPositiveTest() {
        user.setId(1L);
        HelpdeskUserPrincipal principal = new HelpdeskUserPrincipal(user);
        String returnValue = ticketController.ticketView(model, principal);
        verify(model, times(1)).addAttribute(eq("current"), any());
        verify(model, times(1)).addAttribute(eq("tickets"), any());
        Assert.assertEquals("ticket/ticket", returnValue);
    }

    /**
     * Негативный сценарий выполнения метода ticketView() должен вернуть путь к html файлу error
     */
    @Test
    public void ticketViewNegativeTest() {

        verify(model, times(0)).addAttribute(eq("current"), any());
        verify(model, times(0)).addAttribute(eq("tickets"), any());
        String returnValue = ticketController.ticketView(model, null);
        Assert.assertEquals("error/error", returnValue);
    }

    /**
     * Позитивный сценарий выполнения метода ticketInfo() должен вернуть путь к html файлу ticketInfo
     */
    @Test
    public void ticketInfoPositiveTest() {
        user.setId(1L);
        HelpdeskUserPrincipal principal = new HelpdeskUserPrincipal(user);
        ticket.setId(1L);
        String returnValue = ticketController.ticketInfo(1L, model, principal);
        verify(model, times(1)).addAttribute(eq("current"), any());
        verify(model, times(1)).addAttribute(eq("ticketInfo"), any());
        verify(model, times(1)).addAttribute(eq("messages"), any());
        verify(model, times(1)).addAttribute(eq("comment"), any());
        Assert.assertEquals("ticket/ticketInfo", returnValue);
    }

    /**
     * Негативный сценарий выполнения метода ticketInfo() должен вернуть путь к html файлу error
     */
    @Test
    public void ticketInfoNegativeTest() {

        ticket.setId(1L);
        String returnValue = ticketController.ticketInfo(1L, model, null);
        verify(model, times(0)).addAttribute(eq("current"), any());
        verify(model, times(0)).addAttribute(eq("ticketInfo"), any());
        verify(model, times(0)).addAttribute(eq("messages"), any());
        verify(model, times(0)).addAttribute(eq("comment"), any());
        Assert.assertEquals("error/error", returnValue);
    }

    /**
     * Позитивный сценарий выполнения метода ticketNew() должен вернуть путь к html файлу newTicket
     */
    @Test
    public void ticketNewPositiveTest() {
        user.setId(1L);
        HelpdeskUserPrincipal principal = new HelpdeskUserPrincipal(user);
        String returnValue = ticketController.ticketNew(model, principal);
        verify(model, times(1)).addAttribute(eq("current"), any());
        verify(model, times(1)).addAttribute(eq("ticket"), any());
        Assert.assertEquals("ticket/newTicket", returnValue);
    }

    /**
     * Негативный сценарий выполнения метода ticketNew() должен вернуть путь к html файлу error
     */
    @Test
    public void ticketNewNegativeTest() {

        String returnValue = ticketController.ticketNew(model, null);
        verify(model, times(0)).addAttribute(eq("current"), any());
        verify(model, times(0)).addAttribute(eq("ticket"), any());
        Assert.assertEquals("error/error", returnValue);
    }

    /**
     * Позитивный сценарий выполнения метода createTicket()
     * должен вызвать метод ticketService.createTicket()
     * должен вызвать GET запрос на /ticket
     */
    @Test
    public void createTicketPositiveTest() {
        user.setId(1L);
        HelpdeskUserPrincipal principal = new HelpdeskUserPrincipal(user);
        String returnValue = ticketController.createTicket(ticket, principal);
        ticket.setClient(user);
        verify(ticketService, times(1)).createTicket(ticket);
        Assert.assertEquals("redirect:/ticket", returnValue);
    }

    /**
     * Негативный сценарий выполнения метода createTicket()
     * должен вернуть путь к html файлу error
     */
    @Test
    public void createTicketNegativeTest() {

        String returnValue = ticketController.createTicket(ticket, null);
        verify(ticketService, times(0)).createTicket(ticket);
        Assert.assertEquals("error/error", returnValue);
    }

    /**
     * Позитивный сценарий выполнения метода createComment()
     * должен вызвать метод commentService.createComment()
     * должен вызвать GET запрос на /ticket/id
     */
    @Test
    public void createCommentPositiveTest() {
        user.setId(1L);
        HelpdeskUserPrincipal principal = new HelpdeskUserPrincipal(user);
        String returnValue = ticketController.createComment(1L, comment, principal);
        verify(ticketService, times(1)).ticketInfo(1L);
        verify(commentService, times(1)).createComment(any());
        Assert.assertEquals("redirect:/ticket/1", returnValue);
    }

    /**
     * Негативный сценарий выполнения метода createComment()
     * должен вернуть путь к html файлу error
     */
    @Test
    public void createCommentNegativeTest() {

        String returnValue = ticketController.createComment(1L, comment, null);
        verify(ticketService, times(0)).ticketInfo(1L);
        verify(commentService, times(0)).createComment(any());
        Assert.assertEquals("error/error", returnValue);
    }
}
