package ru.edu.helpdesk.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.edu.helpdesk.entity.Title;
import ru.edu.helpdesk.entity.User;
import ru.edu.helpdesk.entity.UserRole;
import ru.edu.helpdesk.form.UserEditForm;
import ru.edu.helpdesk.repository.TicketRepository;
import ru.edu.helpdesk.repository.TitleRepository;
import ru.edu.helpdesk.repository.UserRepository;

import javax.persistence.EntityNotFoundException;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/**
 * AdminServiceImpl Test
 */
@ExtendWith(MockitoExtension.class)
public class AdminServiceImplTest {

    @InjectMocks
    private AdminServiceImpl adminService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private TitleRepository titleRepository;

    @Mock
    private TicketRepository ticketRepository;

    private final User ADMIN = new User(1L, "admin", "admin", "admin", "admin", UserRole.ADMIN);
    private final User SUPPORT = new User(2L, "support", "support", "support", "support", UserRole.SUPPORT);
    private final User USER1 = new User(3L, "user1", "user1", "user1", "user1", UserRole.USER);
    private final User USER2 = new User(4L, "user2", "user2", "user2", "user2", UserRole.USER);
    private final List<User> USERS = Arrays.asList(ADMIN, SUPPORT, USER1, USER2);
    private final Title PC = new Title(1L, "Проблема с PC", false);
    private final Title NETWORK = new Title(1L, "Проблема с NETWORK", false);
    private final Title PRINTER = new Title(1L, "Проблема с PRINTER", false);
    private final List<Title> TITLES = Arrays.asList(PC, NETWORK, PRINTER);

    /**
     * Проверка выполнения метода allUsers()
     */
    @Test
    public void testAllUsers() {
        when(userRepository.findAll()).thenReturn(USERS);

        assertIterableEquals(USERS, adminService.allUsers());
    }

    /**
     * Проверка выполнения метода saveUser()
     */
    @Test
    public void testSaveUser() {
        adminService.saveUser(ADMIN);
        verify(userRepository, times(1)).save(ADMIN);
    }

    /**
     * Проверка выполнения метода getUserByUsername()
     */
    @Test
    public void testGetUserByUsername() {
        when(userRepository.findByLogin(ADMIN.getLogin())).thenReturn(ADMIN);

        assertEquals(ADMIN, adminService.getUserByUsername(ADMIN.getLogin()));
    }

    /**
     * Негативный сценарий выполнения метода getUserById()
     */
    @Test
    public void testGetUserByUsernameNegative() {
        when(userRepository.findByLogin("null")).thenReturn(null);

        assertNull(adminService.getUserByUsername("null"));
    }

    /**
     * Проверка выполнения метода getUserById()
     */
    @Test
    public void testGetUserById() {
        when(userRepository.getById(ADMIN.getId())).thenReturn(ADMIN);

        assertEquals(ADMIN, adminService.getUserById(ADMIN.getId()));
    }

    /**
     * Негативный сценарий выполнения метода getUserById()
     */
    @Test
    public void testGetUserByIdNegative() {
        when(userRepository.getById(0L)).thenThrow(new EntityNotFoundException());

        assertThrows(EntityNotFoundException.class, () -> adminService.getUserById(0L));
    }

    /**
     * Проверка выполнения метода updateUser()
     */
    @Test
    public void testUpdateUser() {

        User user = new User();
        user.setId(USER1.getId());
        user.setLogin(USER1.getLogin());
        user.setRole(USER1.getRole());
        user.setLastName(USER1.getLastName());
        user.setFirstName(USER1.getFirstName());
        user.setPassword(USER1.getPassword());

        when(userRepository.getById(user.getId())).thenReturn(user);

        UserEditForm userEditForm = new UserEditForm();
        userEditForm.setId(user.getId());
        userEditForm.setLogin(user.getLogin());
        userEditForm.setRole(UserRole.ADMIN);
        userEditForm.setLastName(user.getLastName());
        userEditForm.setFirstName(user.getFirstName());
        userEditForm.setChangePassword(true);
        userEditForm.setNewPassword("123");

        adminService.updateUser(userEditForm);

        verify(userRepository, times(1)).save(any());
        verify(passwordEncoder, times(1)).encode(userEditForm.getNewPassword());
    }

    /**
     * Проверка выполнения метода allUsers()
     */
    @Test
    public void testAllTitles() {
        when(titleRepository.findAllByOrderByNameAsc()).thenReturn(TITLES);

        assertIterableEquals(TITLES, adminService.allTitles());
    }

    /**
     * Проверка выполнения метода getTitleById()
     */
    @Test
    public void testGetTitleById() {
        when(titleRepository.getById(NETWORK.getId())).thenReturn(NETWORK);

        assertEquals(NETWORK, adminService.getTitleById(NETWORK.getId()));
    }

    /**
     * Негативный сценарий выполнения метода getTitleById()
     */
    @Test
    public void testGetTitleByIdNegative() {
        when(titleRepository.getById(0L)).thenThrow(new EntityNotFoundException());

        assertThrows(EntityNotFoundException.class, () -> adminService.getTitleById(0L));
    }

    /**
     * Проверка выполнения метода saveTitle()
     */
    @Test
    public void testSaveTitle() {
        adminService.saveTitle(NETWORK);
        verify(titleRepository, times(1)).save(NETWORK);
    }

    /**
     * Проверка выполнения метода deleteTitleById()
     */
    @Test
    public void testDeleteTitleById() {
        when(ticketRepository.countByTitleId(NETWORK.getId())).thenReturn(0);

        adminService.deleteTitleById(NETWORK.getId());
        verify(ticketRepository, times(1)).countByTitleId(NETWORK.getId());
    }

    /**
     * Негативный сценарий выполнения метода deleteTitleById()
     */
    @Test
    public void testDeleteTitleByIdNegative() {
        when(ticketRepository.countByTitleId(NETWORK.getId())).thenReturn(1);

        assertThrows(RuntimeException.class, () -> adminService.deleteTitleById(NETWORK.getId()));
        verify(ticketRepository, times(1)).countByTitleId(NETWORK.getId());
    }
}