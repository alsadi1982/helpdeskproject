package ru.edu.helpdesk.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import ru.edu.helpdesk.entity.*;
import ru.edu.helpdesk.repository.TicketRepository;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
class SupportServiceImplTest {

    @Autowired
    private SupportService supportService;

    @MockBean
    private TicketRepository ticketRepository;

    private final User SUPPORT = new User(1L, "support", "support", "support", "support", UserRole.SUPPORT);
    private final User USER1 = new User(2L, "user1", "user1", "user1", "user1", UserRole.USER);
    private final Title NETWORK = new Title(1L, "Проблема с NETWORK", false);
    private final Ticket TICKET1 = new Ticket(1L, USER1, SUPPORT, NETWORK, "Проблема с NETWORK", TicketStatus.OPEN, LocalDateTime.now());
    private final List<Ticket> TICKETS = Collections.singletonList(TICKET1);

    /**
     * Проверка выполнения метода workStatusTicket() в классе SupportServiceImpl
     * метод должен менять статус тикета на TicketStatus.WORKING
     */
    @Test
    void testWorkStatusTicket() {
        Ticket ticket = new Ticket();
        Assertions.assertDoesNotThrow(() -> supportService.workStatusTicket(ticket));
        Assertions.assertEquals(TicketStatus.WORKING, ticket.getStatus());
        Assertions.assertNotNull(ticket.getCreationAt());
        Mockito.verify(ticketRepository, Mockito.times(1)).save(ticket);
    }

    /**
     * Проверка выполнения метода completeStatusTicket() в классе SupportServiceImpl
     * метод должен менять статус тикета на TicketStatus.COMPLETED
     */
    @Test
    void completeStatusTicket() {
        Ticket ticket = new Ticket();
        Assertions.assertDoesNotThrow(() -> supportService.completeStatusTicket(ticket));
        Assertions.assertEquals(TicketStatus.COMPLETED, ticket.getStatus());
        Assertions.assertNotNull(ticket.getCreationAt());
        Mockito.verify(ticketRepository, Mockito.times(1)).save(ticket);
    }

    /**
     * Проверка выполнения метода rejectedStatusTicket() в классе SupportServiceImpl
     * метод должен менять статус тикета на TicketStatus.REJECTED
     */
    @Test
    void rejectedStatusTicket() {
        Ticket ticket = new Ticket();
        Assertions.assertDoesNotThrow(() -> supportService.rejectedStatusTicket(ticket));
        Assertions.assertEquals(TicketStatus.REJECTED, ticket.getStatus());
        Assertions.assertNotNull(ticket.getCreationAt());
        Mockito.verify(ticketRepository, Mockito.times(1)).save(ticket);
    }

    /**
     * Проверка метода ticketInfo() - информации по тикету.
     */
    @Test
    void ticketInfo() {
        Mockito.when(ticketRepository.getById(TICKET1.getId())).thenReturn(TICKET1);
        assertEquals(TICKET1, supportService.ticketInfo(TICKET1.getId()));
    }

    /**
     * Проверка метода allTicketByStatus() на возвращения списка со статусом.
     */
    @Test
    void allTicketByStatus() {
        Mockito.when(ticketRepository.getTicketByStatus(TicketStatus.OPEN)).thenReturn(TICKETS);
        assertEquals(TICKETS, supportService.allTicketByStatus(TicketStatus.OPEN));
    }

    /**
     * Проверка метода allTicketByStatus() на возвращения списка тикетов по статусу.
     */
    @Test
    void allTickets() {
        Mockito.when(ticketRepository.findAll()).thenReturn(TICKETS);
        assertEquals(TICKETS, supportService.allTickets());
    }

    /**
     * Проверка метода allTicketByStatus() на возвращения списка тикетов юзера.
     */
    @Test
    void allTicketsByUser() {
        Mockito.when(ticketRepository.getTicketBySupport(USER1)).thenReturn(TICKETS);
        assertEquals(TICKETS, supportService.allTicketsByUser(USER1));
    }

    /**
     * Проверка метода takeTicket(), должен менять суппорта по тикету.
     */
    @Test
    void takeTicket() {
        Mockito.when(ticketRepository.save(TICKET1)).thenReturn(TICKET1);
        TICKET1.setSupport(new User());
        assertEquals(TICKET1, supportService.takeTicket(TICKET1, SUPPORT));
        assertEquals(SUPPORT, TICKET1.getSupport());
    }
}